# mobilelocker-veeva-vault

# Deployment Instructions

Add this to your `composer.json` `repositories` section:

~~~
repositories: {
    {
    "type": "vcs",
    "url": "git@bitbucket.org:vorenusventures/mobilelocker-veeva-vault.git"
    }
}
~~~

Checkout the project:

~~~bash

git clone git@bitbucket.org:vorenusventures/mobilelocker-veeva-vault.git
~~~

## Installation

You can install the package via composer:

```bash
composer require mobilelocker/mobilelocker-veeva-vault
```

## Use in a Laravel project

You can publish and run the migrations with:

```bash
php artisan vendor:publish --provider="Mobilelocker\Veeva\Vault\VeevaVaultServiceProvider" --tag="mobilelocker-veeva-vault-migrations"
php artisan migrate
```

You can publish the Laravel config file with:
```bash
php artisan vendor:publish --provider="Mobilelocker\Veeva\Vault\VeevaVaultServiceProvider" --tag="mobilelocker-veeva-vault-config"
```

This is the contents of the published config file:

```php
<?php
return [
    'default' => env('VEEVA_VAULT_CONNECTION', 'default'),

    'connections' => [
        [
            'id' => 'default',
            'name' => 'Default Vault Connection',
            'is_active' => true,
            'document_fields' => [
                'expiration_date' => 'original_expiration_date__c',
            ],
            'username' => (string)env('VEEVA_VAULT_USERNAME'),
            'password' => (string)env('VEEVA_VAULT_PASSWORD'),
            'base_uri' => (string)env('VEEVA_VAULT_BASE_URI'),
            'api_version' => (string)env('VEEVA_VAULT_VERSION', '20.3'),
            'auth_cache_ttl' => (int)env('VEEVA_VAULT_AUTH_CACHE_TTL', 300),
            'cache_ttl' => (int)env('VEEVA_VAULT_CACHE_TTL', 300),
            'query' => '',
        ],
    ],
];
```

## Usage

```php
$vaultClient = new \Mobilelocker\Veeva\Vault\VeevaVaultClient();
$doc = $vaultClient->forConnection('default')->retrieveDocument(123456);
```

## Testing

```bash
composer test
```

## Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

# Changelog

All notable changes to `mobilelocker-veeva-vault` will be documented in this file.

## 1.0.0 - 2021-XX-XX

- initial release

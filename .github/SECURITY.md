# Security Policy

If you discover any security related issues, please email mark@mobilelocker.com instead of using the issue tracker.

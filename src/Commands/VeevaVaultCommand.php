<?php

namespace Mobilelocker\Veeva\Vault\Commands;

use Illuminate\Console\Command;
use Mobilelocker\Veeva\Vault\VeevaVaultClient;

class VeevaVaultCommand extends Command
{
    public $signature = 'vault';

    public $description = 'My command';
    /**
     * @var VeevaVaultClient
     */
    private VeevaVaultClient $client;

    /**
     * VeevaVaultCommand constructor.
     * @param VeevaVaultClient $client
     */
    public function __construct(VeevaVaultClient $client)
    {
        parent::__construct();
        $this->client = $client;
    }


    public function handle()
    {
        $this->comment('All done');
    }
}

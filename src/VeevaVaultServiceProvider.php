<?php

namespace Mobilelocker\Veeva\Vault;

use Mobilelocker\Veeva\Vault\Commands\VeevaVaultCommand;
use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;

class VeevaVaultServiceProvider extends PackageServiceProvider
{
    public function configurePackage(Package $package): void
    {
        /*
         * This class is a Package Service Provider
         *
         * More info: https://github.com/spatie/laravel-package-tools
         */
        $package
            ->name('mobilelocker-veeva-vault')
            ->hasConfigFile('veeva_vault')
            //->hasViews()
            //->hasMigration('create_laravel_veeva_vault_table')
            ->hasCommands([
                VeevaVaultCommand::class,
            ]);
    }
}

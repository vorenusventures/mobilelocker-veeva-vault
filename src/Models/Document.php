<?php


namespace Mobilelocker\Veeva\Vault\Models;

use ArrayAccess;
use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

/**
 * MLD-1685
 * @see https://dev.to/nvio/laravel-greatest-trick-revealed-magic-methods-31om
 * Class Document
 * @package App\Models\Veeva\Vault
 */
class Document implements ArrayAccess, Arrayable, Jsonable
{
    protected array $attributes;

    const RENDITION_TYPE_VIEWABLE = 'viewable_rendition__v';
    const RENDITION_TYPE_VIDEO = 'video_rendition__v';

    // MLD-2964
    const STATUS_APPROVED = 'Approved';
    const STATUS_APPROVED_FOR_DISTRIBUTION = 'Approved for Distribution';
    const STATUS_APPROVED_FOR_USE = 'Approved for Use'; // MLD-3358
    const STATUS_APPROVED_FOR_DISSEMINATION = 'Approved for Dissemination'; // MLD-3950
    const STATUS_READY_FOR_APPROVED_FOR_DISTRIBUTION = 'Ready for Approved for Distribution'; // MLD-6615 United Therapeutics
    const STATUS_EXPIRED = 'Expired'; // MLD-3358
    const STATUS_OBSOLETE = 'Obsolete'; // MLD-4491
    const STATUS_WITHDRAWN = 'Withdrawn';

    // MLD-5315
    const ML_REQUIRED_ATTRIBUTES = [
        'ml_folder__c',
        'ml_groups__c',
        'ml_internal_use_only__c',
        'ml_can_be_emailed__c',
        'ml_can_be_downloaded_by_user__c',
        'ml_can_be_printed_by_user__c',
        'ml_can_be_downloaded_by_viewer__c',
        'ml_can_be_printed_by_viewer__c',
    ];

    const ML_OPTIONAL_ATTRIBUTES = [
        'ml_business_friendly_name__c',
        'ml_alias__c',
        'ml_email_template__c',
        'ml_labels__c', // MLD-6019
    ];

    /**
     * Document constructor.
     */
    public function __construct(array $document)
    {
        $this->attributes = $document;
    }

    public function getID(): int
    {
        return (int) $this->getAttribute('id');
    }

    public function getClassification(): ?string
    {
        return (string) $this->getAttribute('classification__v');
    }

    /**
     * This is an alias
     * @return int
     */
    public function getDocID(): int
    {
        return $this->getID();
    }

    public function getDocumentCreationDate(): ?Carbon
    {
        return $this->getRequiredDateAttribute('document_creation_date__v');
    }

    public function getDocumentNumber(): string
    {
        return (string) $this->getAttribute('document_number__v');
    }

    public function getExpirationDate(string $key = 'original_expiration_date__c', string $timezone = 'UTC'): ?Carbon
    {
        return $this->getOptionalDateAttribute($key, $timezone);
    }

    public function getFilename(): string
    {
        return (string) $this->getAttribute('filename__v');
    }

    public function getExtension(): string
    {
        return pathinfo($this->getFilename(), PATHINFO_EXTENSION);
    }

    public function getDownloadFilename(): string
    {
        return sprintf('%s_%s_%s_%s.%s', $this->getID(), $this->getMajorVersionNumber(), $this->getMinorVersionNumber(), $this->getMD5Checksum(), $this->getExtension());
    }

    /**
     * MLD-3306
     * @return array
     */
    public function getProductIDs(): array
    {
        $productIDs = $this->getAttribute('product__v');
        if (!is_array($productIDs)) {
            return [];
        }
        return $productIDs;
    }

    /**
     * The mimetype of the file
     * @return string
     */
    public function getFormat(): string
    {
        return (string) $this->getAttribute('format__v');
    }

    /**
     * MLD-2485
     * @return bool
     */
    public function isVideo(): bool
    {
        // MLD-3120
        return Str::startsWith($this->getFormat(), 'video/');
    }

    /**
     * MLD-2634
     * @return bool
     */
    public function isPowerPoint(): bool
    {
        // MLD-6004 - support macro-enabled PowerPoint files (PPTM)
        $mimeTypes = [
            'application/vnd.ms-powerpoint.presentation.macroenabled.12',
            'application/vnd.openxmlformats-officedocument.presentationml.presentation',
            'application/vnd.ms-powerpoint',
        ];
        return in_array($this->getFormat(), $mimeTypes);
    }

    public function isExcel(): bool
    {
        $mimeTypes = [
            'application/vnd.ms-excel',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        ];
        return in_array($this->getFormat(), $mimeTypes);
    }

    public function isWordDocument(): bool
    {
        $mimeTypes = [
            'application/msword',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
        ];
        return in_array($this->getFormat(), $mimeTypes);
    }

    /**
     * MLD-2485
     * @return bool
     */
    public function isPDF(): bool
    {
        return $this->getFormat() === 'application/pdf';
    }

    public function getGlobalID(): string
    {
        return (string) $this->getAttribute('global_id__sys');
    }

    public function getMajorVersionNumber(): int
    {
        return (int) $this->getAttribute('major_version_number__v');
    }

    public function getMinorVersionNumber(): int
    {
        return (int) $this->getAttribute('minor_version_number__v');
    }

    public function getMD5Checksum(): string
    {
        return (string) $this->getAttribute('md5checksum__v');
    }

    public function getName(): string
    {
        return (string) $this->getAttribute('name__v');
    }

    public function getPages(): int
    {
        return (int) $this->getAttribute('pages__v');
    }

    /**
     * File size in bytes
     * @return int
     */
    public function getSize(): int
    {
        return (int) $this->getAttribute('size__v');
    }

    /**
     * MLD-3358
     * @return bool
     */
    public function isStatusExpired(): bool
    {
        return Document::STATUS_EXPIRED === $this->getStatus();
    }

    /**
     * MLD-2964
     * @return bool
     */
    public function isStatusWithdrawn(): bool
    {
        return Document::STATUS_WITHDRAWN === $this->getStatus();
    }

    /**
     * MLD-4491
     * @return bool
     */
    public function isStatusObsolete(): bool
    {
        return Document::STATUS_OBSOLETE === $this->getStatus();
    }

    /**
     * MLD-2964
     * @return bool
     */
    public function isApproved(): bool
    {
        return $this->isStatusApproved()
            || $this->isStatusApprovedForDissemination()
            || $this->isStatusApprovedForDistribution()
            || $this->isStatusApprovedForUse()
            || $this->isStatusReadyForApprovedForDistribution(); // MLD-6615
    }

    /**
     * MLD-2964
     * @return bool
     */
    public function isStatusApproved(): bool
    {
        return Document::STATUS_APPROVED === $this->getStatus();
    }

    /**
     * MLD-2964
     * @return bool
     */
    public function isStatusApprovedForDistribution(): bool
    {
        return Document::STATUS_APPROVED_FOR_DISTRIBUTION === $this->getStatus();
    }

    /**
     * MLD-3950
     * @return bool
     */
    public function isStatusApprovedForDissemination(): bool
    {
        return Document::STATUS_APPROVED_FOR_DISSEMINATION === $this->getStatus();
    }

    /**
     * MLD-6615
     * @return bool
     */
    public function isStatusReadyForApprovedForDistribution(): bool
    {
        return Document::STATUS_READY_FOR_APPROVED_FOR_DISTRIBUTION === $this->getStatus();
    }

    /**
     * MLD-3358
     * @return bool
     */
    public function isStatusApprovedForUse(): bool
    {
        return Document::STATUS_APPROVED_FOR_USE === $this->getStatus();
    }

    /**
     * "Approved for Distribution", etc.
     * @return string
     */
    public function getStatus(): string
    {
        return (string) $this->getAttribute('status__v');
    }

    public function getSubtype(): ?string
    {
        return (string) $this->getAttribute('subtype__v');
    }

    public function getTitle(): ?string
    {
        return (string) $this->getAttribute('title__v');
    }

    public function getTitleOrName(): ?string
    {
        return $this->getTitle() ? $this->getTitle() : $this->getName();
    }

    public function getTitleAndVersion(): ?string
    {
        return sprintf('%s v%s.%s', $this->getTitle() ?? $this->getName(), $this->getMajorVersionNumber(), $this->getMinorVersionNumber());
    }

    public function getType(): ?string
    {
        return (string) $this->getAttribute('type__v');
    }

    public function getVersionID(): string
    {
        return (string) $this->getAttribute('version_id');
    }

    public function getVersionCreationDate(): Carbon
    {
        return $this->getRequiredDateAttribute('version_creation_date__v');
    }

    public function getVersionModifiedDate(): Carbon
    {
        return $this->getRequiredDateAttribute('version_modified_date__v');
    }

    public function isBinder(): bool
    {
        return (bool) $this->getAttribute('binder__v');
    }

    public function isLocked(): bool
    {
        return (bool) $this->getAttribute('locked__v');
    }

    /**
     * MLD-5315
     * @see https://chat.openai.com/c/04ee085b-6108-4edb-ac9e-0ece2097a906
     * @return bool
     */
    public function hasMobileLockerAttributes(): bool
    {
        // Flip the keys to use them as array keys for comparison
        $requiredKeysFlipped = array_flip(self::ML_REQUIRED_ATTRIBUTES);

        // Compare the keys
        $missingKeys = array_diff_key($requiredKeysFlipped, $this->attributes);

        // Check if any keys are missing

        // Returns true if all keys exist, false otherwise
        return empty($missingKeys);
    }

    /**
     * MLD-5315
     * @return string|null
     */
    public function getBusinessFriendlyName(): ?string
    {
        if ($this->hasMobileLockerAttributes()) {
            return (string) $this->getAttribute('ml_business_friendly_name__c');
        }
        return null;
    }

    public function getAlias(): ?string
    {
        if ($this->hasMobileLockerAttributes()) {
            return (string) $this->getAttribute('ml_alias__c');
        }
        return null;
    }

    public function getFolderPath(): ?string
    {
        if ($this->hasMobileLockerAttributes()) {
            // Remove leading and trailing slashes
            $folderPathValue = $this->getAttribute('ml_folder__c');
            if (is_array($folderPathValue) && empty($folderPathValue) === false) { // The field could be an array.
                $path = trim($folderPathValue[0], '/\\');
                return str_ireplace('\\', '/', $path);
            } elseif (is_string($folderPathValue)) {
                $path = trim($folderPathValue, '/\\');
                return str_ireplace('\\', '/', $path);
            }
        }
        return null;
    }

    public function getGroups(): ?array
    {
        if ($this->hasMobileLockerAttributes()) {
            return (array) $this->getAttribute('ml_groups__c');
        }
        return null;
    }

    public function getEmailTemplates(): ?array
    {
        if ($this->hasMobileLockerAttributes()) {
            return (array) $this->getAttribute('ml_email_template__c');
        }
        return null;
    }

    public function getEmailTemplate(): ?string
    {
        $templates = $this->getEmailTemplates();
        if (is_array($templates) && count($templates) > 0) {
            return $templates[0];
        }
        return null;
    }

    /**
     * MLD-6019
     * @return mixed
     */
    public function getLabels(): mixed
    {
        return $this->getAttribute('ml_labels__c');
    }

    public function isInternalUseOnly(): ?bool
    {
        if ($this->hasMobileLockerAttributes()) {
            return $this->getBoolean($this->getAttribute('ml_internal_use_only__c'));
        }
        return null;
    }

    private function getBoolean($value): bool
    {
        return filter_var($value, FILTER_VALIDATE_BOOL);
    }

    public function canBeDownloadedByUser(): ?bool
    {
        if ($this->hasMobileLockerAttributes()) {
            return $this->getBoolean($this->getAttribute('ml_can_be_downloaded_by_user__c'));
        }
        return null;
    }

    public function canBeEmailed(): ?bool
    {
        if ($this->hasMobileLockerAttributes()) {
            return $this->getBoolean($this->getAttribute('ml_can_be_emailed__c'));
        }
        return null;
    }

    public function canBePrintedByUser(): ?bool
    {
        if ($this->hasMobileLockerAttributes()) {
            return $this->getBoolean($this->getAttribute('ml_can_be_printed_by_user__c'));
        }
        return null;
    }

    public function canBeDownloadedByViewer(): ?bool
    {
        if ($this->hasMobileLockerAttributes()) {
            return $this->getBoolean($this->getAttribute('ml_can_be_downloaded_by_viewer__c'));
        }
        return null;
    }

    public function canBePrintedByViewer(): ?bool
    {
        if ($this->hasMobileLockerAttributes()) {
            return $this->getBoolean($this->getAttribute('ml_can_be_printed_by_viewer__c'));
        }
        return null;
    }

    protected function getOptionalDateAttribute(string $key, string $timezone = 'UTC'): ?Carbon
    {
        try {
            // MLD-2784 if the date field is null, return NULL instead of letting Carbon return the current date.
            $value = $this->getAttribute($key);
            if (is_null($value)) {
                return null;
            }
            return Carbon::parse($value, $timezone)->setTimezone('UTC');
        } catch (Exception $e) {
            return null;
        }
    }

    protected function getRequiredDateAttribute(string $key): Carbon
    {
        return Carbon::parse($this->getAttribute($key));
    }

    /**
     * MLD-5315
     * @param  string  $key
     * @return bool
     */
    public function hasAttribute(string $key): bool
    {
        return Arr::has($this->attributes, $key);
    }

    protected function getAttribute(string $key)
    {
        if (!$key) {
            return null;
        }
        return Arr::get($this->attributes, $key);
    }

    public function __get($name)
    {
        return $this->getAttribute($name);
    }

    public function toArray(): array
    {
        return $this->attributes;
    }

    /**
     * Convert the model instance to JSON.
     *
     * @param  int  $options
     * @return string
     */
    public function toJson($options = 0): string
    {
        return json_encode($this->jsonSerialize(), $options);
    }

    /**
     * Convert the object into something JSON serializable.
     *
     * @return array
     */
    public function jsonSerialize(): array
    {
        return $this->toArray();
    }

    public function offsetExists($offset): bool
    {
        return isset($this->attributes[$offset]);
    }

    public function offsetGet($offset)
    {
        return $this->attributes[$offset] ?? null;
    }

    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->attributes[] = $value;
        } else {
            $this->attributes[$offset] = $value;
        }
    }

    public function offsetUnset($offset)
    {
        unset($this->attributes[$offset]);
    }
}

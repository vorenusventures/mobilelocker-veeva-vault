<?php


namespace Mobilelocker\Veeva\Vault\Models;

use ArrayAccess;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;
use Illuminate\Support\Arr;

/**
 * MLD-1685
 * Class Connection
 * @package Mobilelocker\Veeva\Vault\Models
 */
class Connection implements ArrayAccess, Arrayable, Jsonable
{

    protected array $attributes;

    // MLD-4217
    const NAME_ONLY_SYNC_METHOD = 'NameOnly';
    const TITLE_NAME_SYNC_METHOD = 'TitleName';

    /**
     * Configuration constructor.
     * @param  array  $attributes
     */
    public function __construct(array $attributes)
    {
        $this->attributes = $attributes;
        $this->validate();
    }

    protected function validate()
    {

    }

    public function getID(): string
    {
        return (string) $this->getAttribute('id');
    }

    public function getName(): string
    {
        return (string) $this->getAttribute('name');
    }

    public function isActive(): bool
    {
        return (bool) $this->getAttribute('is_active');
    }

    /**
     * MLD-5315
     * @return bool
     */
    public function hasMobileLockerFields(): bool
    {
        return $this->getBoolean($this->getAttribute('has_mobilelocker_fields'));
    }

    /**
     * MLD-2964 return TRUE if the value is one of the approved status codes for status__v field.
     * @param  string  $value
     * @return bool
     */
    public function isApprovedStatusCode(string $value): bool
    {
        return in_array($value, $this->getApprovedStatusCodes());
    }

    /**
     * MLD-2964 - return the list of "Approved" values for the status__v field.
     * @return array
     */
    public function getApprovedStatusCodes(): array
    {
        return array_merge([
            Document::STATUS_APPROVED,
            Document::STATUS_APPROVED_FOR_DISTRIBUTION,
            Document::STATUS_APPROVED_FOR_DISSEMINATION, // MLD-3950
            Document::STATUS_APPROVED_FOR_USE, // MLD-3950
            Document::STATUS_READY_FOR_APPROVED_FOR_DISTRIBUTION, // MLD-6615
        ], $this->getAdditionalApprovedStatusCodes());
    }

    public function getAdditionalApprovedStatusCodes(): array
    {
        return (array) $this->getAttribute('additional_approved_status_codes', []);
    }

    public function getDocumentFields(): array
    {
        return (array) $this->getAttribute('document_fields');
    }

    public function getDocumentField(string $field): ?string
    {
        return (string) Arr::get($this->getDocumentFields(), $field);
    }

    public function getUsername(): string
    {
        return (string) $this->getAttribute('username');
    }

    public function getPassword(): string
    {
        return (string) $this->getAttribute('password');
    }

    public function getBaseURI(): string
    {
        return (string) $this->getAttribute('base_uri');
    }

    public function getAPIVersion(): string
    {
        return (string) $this->getAttribute('api_version');
    }

    public function getEndpoint(): string
    {
        return sprintf('%s/api/v%s/', $this->getBaseURI(), $this->getAPIVersion());
    }

    public function getAuthCacheTTL(): int
    {
        return (int) $this->getAttribute('auth_cache_ttl');
    }

    public function getCacheTTL(): int
    {
        return (int) $this->getAttribute('auth_cache_ttl');
    }

    public function getQuery(): string
    {
        return (string) $this->getAttribute('query');
    }

    /**
     * Return the query_fields array
     * MLD-2753
     * @return array|null
     */
    public function getQueryFields(): ?array
    {
        return $this->getAttribute('query_fields');
    }

    /**
     * Return the query fields as a string
     * MLD-2753
     * @return string|null
     */
    public function getQueryFieldsString(): ?string
    {
        if ($fields = $this->getQueryFields()) {
            return implode(', ', $fields);
        }
        return null;
    }

    /**
     * MLD-6133
     * @return array
     */
    public function getGroupNames(): array
    {
        $groups = $this->getAttribute('groups', []);
        if (!is_array($groups)) {
            $groups = [];
        }
        return $groups;
    }

    /**
     * MLD-4217
     * @return string
     */
    public function getNameSyncMethod(): string
    {
        return (string) $this->getAttribute('name_sync_method', self::TITLE_NAME_SYNC_METHOD);
    }

    /**
     * MLD-4217
     * @return bool
     */
    public function isNameSyncMethodTitleName(): bool
    {
        return $this->getNameSyncMethod() === self::TITLE_NAME_SYNC_METHOD;
    }

    /**
     * MLD-42117
     * @return bool
     */
    public function isNameSyncMethodNameOnly(): bool
    {
        return $this->getNameSyncMethod() === self::NAME_ONLY_SYNC_METHOD;
    }

    /**
     * MLD-6542 Return the document_rendition_type value. If not present, the default "viewable_rendition__v" will be returned.
     * @return string
     */
    public function getDocumentRenditionType(): string
    {
        return $this->getAttribute('document_rendition_type', Document::RENDITION_TYPE_VIEWABLE);
    }

    /**
     * MLD-6828 Use Source Files instead of viewable renditions.
     * @return bool
     */
    public function useSourceFiles(): bool
    {
        return (bool) $this->getAttribute('use_source_files', false);
    }

    public function getAttribute(string $key, $default = null)
    {
        if (!$key) {
            return null;
        }
        return Arr::get($this->attributes, $key, $default);
    }

    public function __get($name)
    {
        return $this->getAttribute($name);
    }

    public function toArray(): array
    {
        return $this->attributes;
    }

    /**
     * Convert the model instance to JSON.
     *
     * @param  int  $options
     * @return string
     */
    public function toJson($options = 0): string
    {
        return json_encode($this->jsonSerialize(), $options);
    }

    /**
     * Convert the object into something JSON serializable.
     *
     * @return array
     */
    public function jsonSerialize(): array
    {
        return $this->toArray();
    }

    public function offsetExists($offset): bool
    {
        return isset($this->attributes[$offset]);
    }

    public function offsetGet($offset)
    {
        return $this->attributes[$offset] ?? null;
    }

    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->attributes[] = $value;
        } else {
            $this->attributes[$offset] = $value;
        }
    }

    public function offsetUnset($offset)
    {
        unset($this->attributes[$offset]);
    }

    private function getBoolean($value): bool
    {
        return filter_var($value, FILTER_VALIDATE_BOOL);
    }
}

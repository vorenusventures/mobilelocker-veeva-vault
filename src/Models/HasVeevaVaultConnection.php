<?php


namespace Mobilelocker\Veeva\Vault\Models;


interface HasVeevaVaultConnection
{
    public function isVaultEnabled(): bool;

    public function getVaultConnectionID(): ?string;

    /**
     * TRUE if Veeva is enabled AND there is a connection.
     * @return bool
     */
    public function usesVeevaVault(): bool;
}

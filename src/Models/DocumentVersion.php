<?php

namespace Mobilelocker\Veeva\Vault\Models;

use ArrayAccess;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;
use Illuminate\Support\Arr;

/**
 * MLD-2964
 * @see https://developer.veevavault.com/api/21.3/#retrieve-document
 */
class DocumentVersion implements ArrayAccess, Arrayable, Jsonable
{
    protected array $attributes;

    /**
     * DocumentVersion constructor.
     */
    public function __construct(array $document)
    {
        $this->attributes = $document;
    }

    public function isSameVersion(int $majorVersion, int $minorVersion): bool
    {
        return $this->getMajorVersionNumber() === $majorVersion
            && $this->getMinorVersionNumber() === $minorVersion;
    }

    public function getMajorVersionNumber(): int
    {
        $version = $this->getNumberParts();
        return $version[0];
    }

    public function getMinorVersionNumber(): int
    {
        $version = $this->getNumberParts();
        return $version[1];
    }

    protected function getNumberParts(): array
    {
        $parts = explode('.', $this->getNumber());
        return [
            (int) $parts[0],
            (int) $parts[1],
        ];
    }

    /**
     * This returns a string like "3.1", "3.2", "3.3", etc.
     * @return string
     */
    public function getNumber(): string
    {
        return (string) $this->getAttribute('number');
    }

    /**
     * This returns a URL like "https://myvault.veevavault.com/api/v21.3/objects/documents/534/versions/0/1"
     * @return string
     */
    public function getValue(): string
    {
        return (string) $this->getAttribute('value');
    }

    protected function getAttribute(string $key)
    {
        if (!$key) {
            return null;
        }
        return Arr::get($this->attributes, $key);
    }

    public function __get($name)
    {
        return $this->getAttribute($name);
    }

    public function toArray(): array
    {
        return $this->attributes;
    }

    /**
     * Convert the model instance to JSON.
     *
     * @param  int  $options
     * @return string
     */
    public function toJson($options = 0): string
    {
        return json_encode($this->jsonSerialize(), $options);
    }

    /**
     * Convert the object into something JSON serializable.
     *
     * @return array
     */
    public function jsonSerialize(): array
    {
        return $this->toArray();
    }

    public function offsetExists($offset): bool
    {
        return isset($this->attributes[$offset]);
    }

    public function offsetGet($offset)
    {
        return $this->attributes[$offset] ?? null;
    }

    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->attributes[] = $value;
        } else {
            $this->attributes[$offset] = $value;
        }
    }

    public function offsetUnset($offset)
    {
        unset($this->attributes[$offset]);
    }
}

<?php

namespace Mobilelocker\Veeva\Vault;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Mobilelocker\Veeva\Vault\VeevaVault
 */
class VeevaVaultFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'mobilelocker-veeva-vault';
    }
}

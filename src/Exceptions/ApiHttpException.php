<?php


namespace Mobilelocker\Veeva\Vault\Exceptions;


class ApiHttpException extends VeevaHttpException
{

    private array $result;

    /**
     * ApiFailureHttpException constructor.
     * @param int $statusCode
     * @param array $result
     */
    public function __construct(int $statusCode, array $result)
    {
        $message = sprintf('%s: %s', data_get($result, 'errors.0.type'), data_get($result, 'errors.0.message'));
        parent::__construct($statusCode, $message);
        $this->result = $result;
    }

    /**
     * @return array
     */
    public function getResult(): array
    {
        return $this->result;
    }

    public function getErrors(): array
    {
        return data_get($this->result, 'errors', []);
    }

    public function getErrorType(): string
    {
        return data_get($this->result, 'errors.0.type');
    }

    public function getErrorMessage(): string
    {
        return data_get($this->result, 'errors.0.message');
    }
}

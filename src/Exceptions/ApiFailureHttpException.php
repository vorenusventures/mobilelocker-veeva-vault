<?php


namespace Mobilelocker\Veeva\Vault\Exceptions;


class ApiFailureHttpException extends ApiHttpException
{
    public function __construct(array $result)
    {
        $statusCode = 400;
        parent::__construct($statusCode, $result);
    }

    public function report(): bool
    {
        logger()->debug(sprintf('%s: %s', class_basename(self::class), $this->getErrorMessage()));
        return false;
    }
}

<?php


namespace Mobilelocker\Veeva\Vault\Exceptions;

use RuntimeException;

/**
 * MLD-1685
 * Class ConnectionNotFoundException
 * @package Mobilelocker\Veeva\Vault\Exceptions
 */
class ConnectionNotFoundException extends RuntimeException
{
    public function __construct(string $connectionID)
    {
        $message = sprintf('Veeva Vault connection %s was not found in the configuration file.', $connectionID);
        parent::__construct($message);
    }
}

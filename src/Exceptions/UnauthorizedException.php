<?php

namespace Mobilelocker\Veeva\Vault\Exceptions;

use Throwable;

/**
 * Class UnauthorizedException
 * @package App\Exceptions\Veeva
 */
class UnauthorizedException extends VeevaHttpException
{
    //
    public function __construct(
        int $statusCode,
        ?string $message = null,
        Throwable $previous = null,
        array $headers = [],
        ?int $code = 0
    ) {
        parent::__construct($statusCode, $message, $previous, $headers, $code);
    }
}

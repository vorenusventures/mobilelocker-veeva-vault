<?php


namespace Mobilelocker\Veeva\Vault\Exceptions;

/**
 * MLD-1685
 * Class DocumentNotFoundException
 * @package Mobilelocker\Veeva\Vault\Exceptions
 */
class DocumentNotFoundException extends VeevaHttpException
{
    public function __construct(int $docID, \Throwable $previous = null, array $headers = [], ?int $code = 0)
    {
        $message = sprintf('Vault document %s was not found', $docID);
        parent::__construct(404, $message, $previous, $headers, $code);
    }
}

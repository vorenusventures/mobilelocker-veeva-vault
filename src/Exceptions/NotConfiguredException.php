<?php


namespace Mobilelocker\Veeva\Vault\Exceptions;


class NotConfiguredException extends VeevaHttpException
{
    public function __construct(?string $message = null)
    {
        if (!$message) {
            $message = sprintf('Veeva Vault is not configured');
        }
        parent::__construct(400, $message);
    }

}

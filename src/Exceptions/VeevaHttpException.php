<?php

namespace Mobilelocker\Veeva\Vault\Exceptions;

use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class VeevaHttpException
 * @package App\Exceptions\Veeva
 */
class VeevaHttpException extends HttpException
{
    //
}

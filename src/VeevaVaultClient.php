<?php


namespace Mobilelocker\Veeva\Vault;

use Illuminate\Http\Client\PendingRequest;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Mobilelocker\Veeva\Vault\Exceptions\ApiExceptionHttpException;
use Mobilelocker\Veeva\Vault\Exceptions\ApiFailureHttpException;
use Mobilelocker\Veeva\Vault\Exceptions\ConnectionNotFoundException;
use Mobilelocker\Veeva\Vault\Exceptions\UnauthorizedException;
use Mobilelocker\Veeva\Vault\Models\Connection;
use Mobilelocker\Veeva\Vault\Models\Document;
use Mobilelocker\Veeva\Vault\Models\DocumentVersion;

/**
 * @see https://developer.veevanetwork.com/
 * Class VeevaVaultClient
 * @package Mobilelocker\Veeva\Vault
 */
class VeevaVaultClient
{
    private ?Connection $config;

    private const FAILURE = 'FAILURE';
    private const EXCEPTION = 'EXCEPTION';

    /**
     * @throws ConnectionNotFoundException
     */
    public function forConnection(string $connectionID): self
    {
        $connection = $this->getConnection($connectionID);
        return $this->forConfig($connection);
    }

    /**
     * @return Collection|Connection[]
     */
    public function getConnections(): Collection
    {
        return collect(config('veeva_vault.connections', []))->map(fn($config) => new Connection($config));
    }

    /**
     * @throws ConnectionNotFoundException
     */
    public function getConnection(string $connectionID): Connection
    {
        if ($connection = $this->getConnections()->first(function (Connection $connection) use ($connectionID) {
            return $connection->getID() === $connectionID;
        })) {
            return $connection;
        }
        throw new ConnectionNotFoundException($connectionID);
    }

    /**
     * @see https://developer.veevanetwork.com/api/#authentication
     * @param  Connection  $config
     * @return $this
     */
    public function forConfig(Connection $config): self
    {
        $this->config = $config;
        $this->authenticate();

        return $this;
    }

    /**
     * @see https://developer.veevavault.com/api/20.3/#vault-query-language-vql
     * @param  string  $q
     * @param  array  $params
     * @return array
     */
    public function query(string $q, array $params = []): array
    {
        if ($navigate = data_get($params, 'navigate')) {
            return $this->getResult($navigate);
        }
        $uri = sprintf('query');
        $form = [
            'q' => $q,
        ];
        return $this->formPostResult($uri, $form);
    }

    /**
     * @see https://developer.veevavault.com/api/20.3/#retrieve-document-fields
     * @param  array  $params
     * @return Collection
     */
    public function retrieveDocumentFields(array $params = []): Collection
    {
        $uri = 'metadata/objects/documents/properties';
        return collect(Arr::get($this->getResult($uri, $params), 'properties', []));
    }

    /**
     * @see https://developer.veevavault.com/api/20.3/#retrieve-document-types
     * @param  array  $params
     * @return array
     */
    public function retrieveDocumentTypes(array $params = []): array
    {
        $uri = 'metadata/objects/documents/types';
        return $this->getResult($uri, $params);
    }

    /**
     * @see https://developer.veevavault.com/api/20.3/#retrieve-documents
     * @param  array  $params
     */
    public function retrieveDocuments(array $params = []): array
    {
        $query = Arr::only($params, [
            'limit',
            'sort',
            'start',
            'named_filter',
            'scope',
            'search',
            'versionscope',
        ]);
        $uri = 'objects/documents';
        return $this->getResult($uri, $query);
    }

    /**
     * @see https://developer.veevavault.com/api/20.3/#retrieve-document
     * @param  int  $docID
     * @param  array  $params
     * @return Document
     */
    public function retrieveDocument(int $docID, array $params = []): Document
    {
        $uri = sprintf('objects/documents/%s', $docID);
        return new Document(data_get($this->getResult($uri, $params), 'document'));
    }

    /**
     * MLD-2964
     * @see https://developer.veevavault.com/api/21.3/#retrieve-document-versions
     * @param  int  $docID
     * @return Collection
     */
    public function retrieveDocumentVersions(int $docID): Collection
    {
        $uri = sprintf('objects/documents/%s/versions', $docID);
        return collect(data_get($this->getResult($uri), 'versions', []))->map(fn(array $version) => new DocumentVersion($version));
    }

    /**
     * MLD-2964
     * @see https://developer.veevavault.com/api/21.3/#retrieve-document-version
     * @param  int  $docID
     * @param  int  $majorVersion
     * @param  int  $minorVersion
     * @return Document
     */
    public function retrieveDocumentVersion(int $docID, int $majorVersion, int $minorVersion): Document
    {
        $uri = sprintf('objects/documents/%s/versions/%s/%s', $docID, $majorVersion, $minorVersion);
        return new Document(data_get($this->getResult($uri), 'document'));
    }

    /**
     * MLD-2486
     * @see https://developer.veevavault.com/api/20.3/#download-document-file
     * @param  int  $docID
     * @param  string  $sink
     * @return Response
     */
    public function downloadDocumentFile(int $docID, string $sink): Response
    {
        $uri = sprintf('objects/documents/%s/file', $docID);
        return $this->getClient()->sink($sink)->get($uri);
    }

    /**
     * @see https://developer.veevavault.com/api/20.3/#download-document-version-file
     * @param  int  $docID
     * @param  int  $major
     * @param  int  $minor
     * @param  string  $sink
     * @return Response
     */
    public function downloadDocumentVersionFile(int $docID, int $major, int $minor, string $sink): Response
    {
        $uri = sprintf('objects/documents/%s/versions/%s/%s/file', $docID, $major, $minor);
        return $this->getClient()->sink($sink)->get($uri);
    }

    /**
     * @see https://developer.veevavault.com/api/20.3/#retrieve-document-renditions
     * @param  int  $docID
     * @return Response
     */
    public function retrieveDocumentRenditions(int $docID): Response
    {
        $uri = sprintf('objects/documents/%s/renditions', $docID);
        $response = $this->getClient()->get($uri);
        $this->checkResult($response->json());
        return $response;
    }

    /**
     * @see https://developer.veevavault.com/api/20.3/#retrieve-document-version-renditions
     * @param  int  $docID
     * @param  int  $majorVersion
     * @param  int  $minorVersion
     * @return Response
     */
    public function retrieveDocumentVersionRenditions(
        int $docID,
        int $majorVersion,
        int $minorVersion
    ): Response {
        $uri = sprintf('objects/documents/%s/versions/%s/%s/renditions', $docID, $majorVersion, $minorVersion);
        $response = $this->getClient()->get($uri);
        $this->checkResult($response->json());
        return $response;
    }

    /**
     * @see https://developer.veevavault.com/api/20.3/#download-document-version-rendition-file
     * @param  int  $docID
     * @param  int  $major
     * @param  int  $minor
     * @param  string  $renditionType
     * @param  string  $sink
     * @return Response
     */
    public function downloadDocumentRendition(
        int $docID,
        int $major,
        int $minor,
        string $sink,
        string $renditionType = Document::RENDITION_TYPE_VIEWABLE
    ): Response {
        $uri = sprintf('objects/documents/%s/versions/%s/%s/renditions/%s', $docID, $major, $minor, $renditionType);
        $response = $this->getClient()->sink($sink)->get($uri);
        if ($json = $response->json()) {
            $this->checkResult($json);
        }
        return $response;
    }

    /**
     * @see https://developer.veevavault.com/api/20.3/#retrieve-object-collection
     * @param  array  $query
     * @return array
     */
    public function retrieveObjectCollection(array $query = []): array
    {
        $uri = sprintf('metadata/vobjects');
        return $this->getResult($uri, $query);
    }

    /**
     * @see https://developer.veevavault.com/api/20.3/#retrieve-object-metadata
     * @param  array  $query
     * @return array
     */
    public function retrieveProducts(array $query = []): array
    {
        return $this->retrieveVaultObjectRecordCollection('product__v');
    }

    /**
     * MLD-2486
     * Retrieve all object and object field permissions (read, edit, create, delete)
     * assigned to the currently authenticated user.
     * @see https://developer.veevavault.com/api/20.3/#retrieve-my-user-permissions
     * @return Collection
     */
    public function retrieveMyUserPermissions(string $permissionName = null): Collection
    {
        $uri = sprintf('objects/users/me/permissions');
        $query = [];
        if ($permissionName) {
            $query['filter'] = sprintf('name__v::%s', $permissionName);
        }
        return collect(data_get($this->getResult($uri, $query), 'data', []));
    }

    /**
     * @see https://developer.veevavault.com/api/20.3/#retrieve-all-picklists
     * @return Collection
     */
    public function retrievePicklists(): Collection
    {
        $uri = sprintf('objects/picklists');
        return collect(data_get($this->getResult($uri), 'picklists', []));
    }

    /**
     * @see https://developer.veevavault.com/api/20.3/#retrieve-picklist-values
     * @param  string  $picklistName
     * @return Collection
     */
    public function retrievePicklistValues(string $picklistName): Collection
    {
        $uri = sprintf('objects/picklists/%s', $picklistName);
        return collect(data_get($this->getResult($uri), 'picklistValues', []));
    }

    /**
     * Retrieve all metadata configured on a standard or custom Vault Object.
     * @see https://developer.veevavault.com/api/20.3/#retrieve-object-metadata
     * @param  string  $objectName
     * @param  array  $query
     * @return array
     */
    public function retrieveVaultObjectMetadata(string $objectName, array $query = []): array
    {
        $uri = sprintf('metadata/vobjects/%s', $objectName);
        return $this->getResult($uri, $query);
    }

    /**
     * Retrieve all records for a specific Vault Object.
     * @see https://developer.veevavault.com/api/20.3/#retrieve-object-record-collection
     * @param  string  $objectName
     * @param  array  $query
     * @param  bool  $allRecords
     * @return array
     */
    public function retrieveVaultObjectRecordCollection(string $objectName, array $query = []): array
    {
        $uri = sprintf('vobjects/%s', $objectName);
        $response = $this->getResult($uri, $query);
        $nextPage = Arr::get($response, 'responseDetails.next_page');
        $items = collect(Arr::get($response, 'data', []));
        if ($nextPage) {
            $this->retrieveAll($items, $nextPage);
        }
        return $items->toArray();
    }

    /**
     * @see https://developer.veevavault.com/api/20.3/#retrieve-object-record
     * @param  string  $objectName
     * @param  string  $objectRecordID
     * @param  array  $query
     * @return array
     */
    public function retrieveVaultObjectRecord(string $objectName, string $objectRecordID, array $query = []): array
    {
        $uri = sprintf('vobjects/%s/%s', $objectName, $objectRecordID);
        return $this->getResult($uri, $query);
    }

    /**
     * @see https://developer.veevavault.com/api/20.3/#validate-session-id
     * @param  string  $sessionID
     * @return array
     */
    public function validateSessionID(string $sessionID): array
    {
        $uri = sprintf('objects/users/me');
        return $this->getResult($uri);
    }

    public function getResult(string $uri, array $query = []): array
    {
        return $this->checkResult($this->getClient()->get($uri, $query)->json());
    }

    public function formPostResult(string $uri, array $form = []): array
    {
        return $this->checkResult($this->getClient()->asForm()->post($uri, $form)->json());
    }

    protected function checkResult(array $result): array
    {
        $responseStatus = Arr::get($result, 'responseStatus');
        if ($responseStatus === self::FAILURE) {
            throw new ApiFailureHttpException($result);
        } elseif ($responseStatus === self::EXCEPTION) {
            throw new ApiExceptionHttpException($result);
        }
        return $result;
    }

    protected function withOptions(bool $authenticated = false): PendingRequest
    {
        $options = [
            'base_uri' => $this->config->getEndpoint(),
            'headers' => [],
        ];
        if ($authenticated) {
            $options['headers']['Authorization'] = $this->getSessionID();
        }

        return Http::withOptions($options);
    }

    public function getClient(): PendingRequest
    {
        return $this->withOptions(true);
    }

    public function getSessionID(): string
    {
        return data_get($this->authenticate(), 'sessionId');
    }

    /**
     * Authenticate to Veeva and store the response in the cache so it can be used by subsequent attempts
     * @see https://developer.veevanetwork.com/api/#authenticate
     * @see https://developer.veevanetwork.com/docs/#authentication
     * @return array
     */
    protected function authenticate(): array
    {
        $cacheKey = sprintf('veeva_client_%s_auth', $this->config->getID());

        return Cache::tags($this->cacheTags())->remember($cacheKey, $this->config->getAuthCacheTTL(), function () {
            $config = $this->config;
            $data = [
                'username' => $config->getUsername(),
                'password' => $config->getPassword(),
            ];
            $response = $this->withOptions()->asForm()->post('auth', $data);
            $status = $response->status();
            $responseStatus = data_get($response->json(), 'responseStatus');
            if ($responseStatus === 'SUCCESS') {
                return $response->json();
            }
            // Example:
            // {"responseStatus":"FAILURE","responseMessage":"Authentication failed for user [Mark.Mobilelocker_agency@allergan.com]","errors":[{"type":"USERNAME_OR_PASSWORD_INCORRECT","message":"Authentication failed for user: mark.mobilelocker_agency@allergan.com."}],"errorType":"AUTHENTICATION_FAILED"}
            $responseMessage = Arr::get($response->json(), 'responseMessage'); // Authentication failed for user [Mark.Mobilelocker_agency@allergan.com]
            $errorType = Arr::get($response->json(), 'errorType'); // AUTHENTICATION_FAILED
            //$errorMessage = Arr::get($response->json(), 'errors.0.type'); // USERNAME_OR_PASSWORD_INCORRECT
            if ($errorType === 'AUTHENTICATION_FAILED') {
                throw new UnauthorizedException(401, $responseMessage ?? $response->body());
            } elseif ($responseStatus === 'EXCEPTION') {
                throw new UnauthorizedException($status, $responseMessage ?? $response->body());
            } elseif ($responseStatus === 'FAILURE') {
                //$errorType = Arr::get($response->json(), 'errors.0.type');
                throw new UnauthorizedException($status, $responseMessage ?? $response->body());
            }
            throw new UnauthorizedException($status, $response->body());
        });
    }

    public function rawGet(string $uri): Response
    {
        $baseURI = $this->config->getBaseURI();
        $options = [
            'base_uri' => $baseURI,
            'headers' => [
                'Authorization' => $this->getSessionID(),
            ],
        ];

        return Http::withOptions($options)->get($uri);
    }

    /**
     * MLD-2692
     * @param  Collection  $items
     * @param  string  $nextPage
     */
    public function retrieveAll(Collection $items, string $nextPage)
    {
        $response = $this->rawGet($nextPage);
        $objects = Arr::get($response, 'data', []);
        foreach ($objects as $object) {
            $items->push($object);
        }
        if ($nextPage = Arr::get($response, 'responseDetails.next_page')) {
            $this->retrieveAll($items, $nextPage);
        }
    }

    public function flushCache(): self
    {
        Cache::tags($this->cacheTags())->flush();
        return $this;
    }

    protected function cacheTags(): array
    {
        return [
            sprintf('veeva_client_%s', $this->config->getID()),
        ];
    }

    protected function cacheTTL(): int
    {
        return $this->config->getCacheTTL();
    }
}

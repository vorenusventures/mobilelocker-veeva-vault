<?php


namespace Mobilelocker\Veeva\Vault\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class SearchHCPRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @see https://developer.veevanetwork.com/api/#search
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'q' => [
                'required',
                'string',
            ],
            'address_q' => [
                'nullable',
                'string',
            ],
            'types' => [
                'nullable',
                'string',
                Rule::in(['ALL', 'HCO', 'HCP']),
            ],
            'offset' => [
                'nullable',
                'integer',
                'min:0',
            ],
            'limit' => [
                'nullable',
                'integer',
                'min:0',
                'max:100',
            ],
            'sort' => [
                'nullable',
                'string',
            ],
            'sortOrder' => [
                'nullable',
                'string',
                Rule::in(['asc', 'desc']),
            ],
            'states' => [
                'nullable',
                'string',
                Rule::in([
                    'VALID',
                    'DELETED',
                    'UNDER_REVIEW',
                    'MERGED_INTO',
                    'MERGE_INACTIVATED',
                    'MERGE_ADDED',
                    'INVALID',
                ]),
            ],
            'statuses' => [
                'nullable',
                'string',
                Rule::in([
                    'A', // active
                    'C', // closed
                    'D', // deceased
                    'I', // inactive
                    'R', // retired
                    'U', // unknown
                    'X', // unknown license
                ]),
            ],
            'returnFacets' => [
                'boolean',
            ],
            'returnHighlights' => [
                'boolean',
            ],
            'facetCount' => [
                'nullable',
                'integer',
                'min:0',
                'max:500',
            ],
            'includeMasterReulsts' => [
                'boolean',
            ],
            'searchOnlyMaster' => [
                'boolean',
            ],
            'filters' => [
                'nullable',
                'string',
            ],
        ];
    }
}

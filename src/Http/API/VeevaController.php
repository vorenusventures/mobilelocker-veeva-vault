<?php


namespace Mobilelocker\Veeva\Vault\Http\API;

use Illuminate\Support\Facades\Auth;
use Mobilelocker\Veeva\Vault\VeevaVaultClient;

class VeevaController
{
    /**
     * @var VeevaVaultClient
     */
    protected VeevaVaultClient $veevaClient;

    /**
     * VeevaController constructor.
     * @param VeevaVaultClient $veevaClient
     */
    public function __construct(VeevaVaultClient $veevaClient)
    {
        $this->veevaClient = $veevaClient;
    }

    protected function user(string $guard = 'api')
    {
        return Auth::guard($guard)->user();
    }

    /**
     * @param mixed $data
     * @param int $status
     * @param array $headers
     * @param int $options
     * @return \Illuminate\Http\JsonResponse
     */
    protected function json($data = null, int $status = 200, array $headers = [], $options = JSON_PRETTY_PRINT)
    {
        return response()->json($data, $status, $headers, $options);
    }
}

<?php


namespace Mobilelocker\Veeva\Vault\Http\API;

use Mobilelocker\Veeva\Vault\Http\Requests\SearchHCPRequest;

class HCPController extends VeevaController
{
    /**
     * @see https://developer.veevanetwork.com/api/#search
     * @param SearchHCPRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function search(SearchHCPRequest $request)
    {
        $query = $request->only([
            'q',
            'address_q',
            'types',
            'offset',
            'limit',
            'sort',
            'sortOrder',
            'states',
            'statuses',
            'returnFacets',
            'returnHighlights',
            'facetCount',
            'includeMasterResults',
            'searchOnlyMaster',
            'filters',
        ]);
        $hcps = $this->veevaClient->searchHCPs($query);

        return $this->json($hcps);
    }
}

<?php

namespace Mobilelocker\Veeva\Vault\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Mobilelocker\Veeva\Vault\Models\HCP;

class ModelFactory extends Factory
{
    protected $model = HCP::class;

    public function definition()
    {
        return [

        ];
    }
}


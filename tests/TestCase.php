<?php

namespace Mobilelocker\Veeva\Vault\Tests;

use Illuminate\Database\Eloquent\Factories\Factory;
use Mobilelocker\Veeva\Vault\VeevaVaultServiceProvider;
use Orchestra\Testbench\TestCase as Orchestra;

class TestCase extends Orchestra
{
    public function setUp(): void
    {
        parent::setUp();

        Factory::guessFactoryNamesUsing(
            fn (string $modelName) => 'Mobilelocker\\Veeva\\Vault\\Database\\Factories\\'.class_basename($modelName).'Factory'
        );
    }

    protected function getPackageProviders($app)
    {
        return [
            VeevaVaultServiceProvider::class,
        ];
    }

    public function getEnvironmentSetUp($app)
    {
        $app['config']->set('database.default', 'sqlite');
        $app['config']->set('database.connections.sqlite', [
            'driver' => 'sqlite',
            'database' => ':memory:',
            'prefix' => '',
        ]);

        /*
        include_once __DIR__.'/../database/migrations/create_laravel_veeva_vault_table.php.stub';
        (new \CreatePackageTable())->up();
        */
    }
}

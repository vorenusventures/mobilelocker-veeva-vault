<?php

return [
    'default' => env('VEEVA_VAULT_CONNECTION', 'default'),

    'connections' => [
        [
            'id' => 'default',
            'name' => 'Default Vault Connection',
            'is_active' => true,
            'document_fields' => [
                'expiration_date' => 'original_expiration_date__c',
            ],
            'username' => (string)env('VEEVA_VAULT_USERNAME'),
            'password' => (string)env('VEEVA_VAULT_PASSWORD'),
            'base_uri' => (string)env('VEEVA_VAULT_BASE_URI'),
            'api_version' => (string)env('VEEVA_VAULT_VERSION', '20.3'),
            'auth_cache_ttl' => (int)env('VEEVA_VAULT_AUTH_CACHE_TTL', 300),
            'cache_ttl' => (int)env('VEEVA_VAULT_CACHE_TTL', 300),
            'query' => '',
        ],
    ],
];
